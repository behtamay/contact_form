<?php
session_start();
include 'connection.php';
include 'process_form.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Edit</title>
</head>

<body>
    <div class="container">
        <div class="contact_box mb-5">
            <h1 class="input-title text-center">Edit Contact Form</h1>

            <form action="index.php" method="post">

                <div class="mb-2">
                    <label for="firstname">Firstname</label>
                    <input type="hidden" name="id" value="<?=$row->id;?>">
                    <input class="form-control" id="firstname" type="text" name="firstname"
                        value="<?=$row->firstname;?>">
                </div>
                <div class="mb-2">
                    <label for="lastname">Lastname</label>
                    <input class="form-control" id="lastname" type="text" name="lastname" value="<?=$row->lastname;?>">
                    <div class="mb-2">
                        <label for="email">Email</label>
                        <input class="form-control" id="email" type="email" name="email" value="<?=$row->email;?>">
                    </div>
                    <div class="mb-2">
                        <label for="contactno">ContactNo</label>
                        <input class="form-control" id="contactno" type="text" name="contactno"
                            value="<?=$row->contactno;?>">
                    </div>
                    <div class="mb-2">
                        <label for="subject">Subject</label>
                        <input class="form-control" id="subject" type="text" name="subject" value="<?=$row->subject;?>">
                    </div>
                    <div class="float-end">
                        <button type="submit" class="btn btn-info mb-5" name="edit-submit">Submit</button>
                    </div>
            </form>
        </div>
    </div>
</body>

</html>