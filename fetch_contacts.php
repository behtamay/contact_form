<?php
// Database configuration
$db_host = "localhost";
$db_name = "php_pdo";
$db_user = "root";
$db_password = "";


try {
    // Connect to the database using PDO
    $pdo = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Fetch all contacts from the database
    $stmt = $pdo->query("SELECT * FROM contacts");
    $contacts = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Display the contacts
    foreach ($contacts as $contact) {
        echo '<li class="list-group-item">';
        echo '<strong>Firstname:</strong> ' . $contact['firstname'] . ' | ';
        echo '<strong>Lasttname:</strong> ' . $contact['lastname'] . ' | ';
        echo '<strong>Email:</strong> ' . $contact['email'] . ' | ';
        echo '<strong>Contactno:</strong> ' . $contact['contactno'] . ' | ';
        echo '<strong>Subject:</strong> ' . $contact['subject'] . ' | ';
        echo '<strong>Message:</strong> ' . $contact['message'] . ' | ';
        echo '<a href="delete_contact.php?id=' . $contact['id'] . '">Delete</a> | ';
        echo '<a href="update_contact.php?id=' . $contact['id'] . '">Update</a>';
        echo '</li>';
    }
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>