CREATE DATABASE php_pdo;

CREATE TABLE `pdo_tbl` (
 `id` int(100) NOT NULL AUTO_INCREMENT,
 `firstname` varchar(100) NOT NULL,
 `lastname` varchar(100) NOT NULL,
 `email` varchar(100) NOT NULL,
 `contactno` varchar(100) NOT NULL,
 `subject` varchar(100) NOT NULL,
 `message` varchar(100) NOT NULL,
 PRIMARY KEY (`id`)
);