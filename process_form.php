<?php
if (isset($_POST['submit'])) { 
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $contactno = $_POST['contactno'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];
    if($firstname == '' || $lastname == '' || $email == '' || $contactno == '' || $subject == '' || $message == ''){
      echo "<script>alert('Field Required!')</script>";
    }
    else{
      $sql = "INSERT INTO pdo_tbl (firstname, lastname, email,contactno,subject,message)
      VALUES (:firstname, :lastname,  :email,:contactno,:subject,:message)";
      $sql_run = $conn->prepare($sql);
      
      $data = [':firstname' => $firstname,
      ':lastname' => $lastname,
      ':email' => $email,
      ':contactno' => $contactno,
      ':subject' => $subject,
      ':message' => $message,
    ];
      $sql_execute = $sql_run->execute($data);
      if($sql_execute){
        echo "<script>alert('Send message successfully!')</script>";
      }
      else{
        echo "<script>alert('Oops! Send message failed!')</script>";
      }
    }
} 
if(isset($_GET['del'])){
    $id = $_GET['del'];
    $sql = "DELETE FROM pdo_tbl WHERE id = :id";
    $sql_run = $conn->prepare($sql);
    $data = [':id' => $id,
];
$sql_execute = $sql_run->execute($data);
if($sql_execute){
    echo "<script>alert('Delete successfully!')</script>";
  }
  else{
    echo "<script>alert('Oops! Delete failed!')</script>";
  }
}
if(isset($_GET['edit'])){
    $id = $_GET['edit'];
    $query = "SELECT * FROM pdo_tbl WHERE id = :id";
    $stmt = $conn->prepare($query);
    $data = [':id' => $id,
    ];
    $stmt->execute($data);
    $row = $stmt->fetch(PDO::FETCH_OBJ); 
}
if(isset($_POST['edit-submit'])){
    $id = $_POST['id'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $contactno = $_POST['contactno'];
    $subject = $_POST['subject'];
      $sql = "UPDATE pdo_tbl SET firstname=:firstname,lastname=:lastname,email=:email,contactno=:contactno,subject=:subject WHERE id=:id";
      $sql_run = $conn->prepare($sql);
      $data = [':firstname' => $firstname,
      ':lastname' => $lastname,
      ':email' => $email,
      ':contactno' => $contactno,
      ':subject' => $subject,
      ':id' => $id,
    ];
      $sql_execute = $sql_run->execute($data);
      if($sql_execute){
        echo "<script>alert('Update successfully!')</script>";
      }
      else{
        echo "<script>alert('Update failed!')</script>";
      }
}
?>